
import base64
import os
import sys
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.fernet import Fernet


class Vault():
    def __init__(self,filename,password=None):
        self.input_file = filename

        self.password = password


    def create_key(self,password):
        password_provided = password # input password in form of a string
        password = password_provided.encode() # convert to type bytes
        salt = b'=\xed+(2\x16q&\x06\xc9W\xe2[\xb1\xadS' # CHANGE THIS - recommend using a key from os.urandom(16), must be of type bytes
        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=32,
            salt=salt,
            iterations=100000,
            backend=default_backend()
        )
        return base64.urlsafe_b64encode(kdf.derive(password))

    def encrypt_file(self):
        if self.password == None:
            print('Need a password to encrypt.')
            sys.exit(1)
        encrypt_key = self.create_key(self.password)
        input_file = self.input_file
        output_file = f'encrypted/{self.input_file}.encrypted'

        with open(input_file, 'rb') as f:
            data = f.read()

        fernet = Fernet(encrypt_key)
        encrypted = fernet.encrypt(data)

        with open(output_file, 'wb') as f:
            f.write(encrypted)

    def decrypt_file(self,key=None):

        input_file = f'encrypted/{self.input_file}.encrypted'
        output_file = f'decrypted/{self.input_file}.decrypted'

        if key is None:
            if self.password != None:
                decrypt_key = self.create_key(self.password)
            else:
                print('Expecting password, none provided. Provide at least key or password.')
                sys.exit(1)
        else:
            decrypt_key = key

        with open(input_file, 'rb') as f:
            data = f.read()

        fernet = Fernet(decrypt_key)
        try:
            encrypted = fernet.decrypt(data)
        except Exception:
            print('Wrong Password or Key')
            sys.exit(1)

        with open(output_file,'wb') as f:
            f.write(encrypted)
    
    def store_key(self):
        key = self.create_key(self.password)
        file = open('key.key', 'wb')
        file.write(key)
        file.close()

# Vault(filename='test_setup.cfg').decrypt_file('ycJuBBVWSOA1H_6IZkiGHpWBF7ibhxoUzqJkV81pTUo=')
# Vault(filename='test_setup.cfg',password='abc123').store_key()
Vault(filename='README.md').decrypt_file('Z7l-Fk5dJaIZtqB8DhQrWed2a2B2XEWzVdIneZSepl0=')