# example of how to generate a keyu using a password


import base64
import os
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.fernet import Fernet


password_provided = "password" # input password in form of a string
password = password_provided.encode() # convert to type bytes
salt = b'salt_' # CHANGE THIS - recommend using a key from os.urandom(16), must be of type bytes

kdf = PBKDF2HMAC(
    algorithm=hashes.SHA256(),
    length=32,
    salt=salt,
    iterations=100000,
    backend=default_backend()
)

key = base64.urlsafe_b64encode(kdf.derive(password)) # can only use kdf once



# storing keys
# file = open('key.key', 'wb')
# file.write(key)
# file.close()


message = "my deep dark secret".encode()
f = Fernet(key)
encrypted = f.encrypt(message)

file = open('encrypted.txt', 'wb')
file.write(encrypted)
file.close()


encrypted_message = b"gAAAAABeL4P7Kgkd9UK4Z_6xXFfzObbYSFv1jthyYhppecMARNjJk6EtaiYenvT1VwiFdew5ZIh50e2o1PrNwzu9-2rkPjOCtKAu8DZMvPtonHmneXTOUCU="

decrypted = f.decrypt(encrypted_message)
print(decrypted)